<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('courses', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('name', 200);
            $table->text('description', 2000);
            $table->bigInteger('studyLoad');
            $table->enum('level', ['bachelor', 'master', 'doctoral'])->default('bachelor');
            $table->integer('courseLengthInDays');
            $table->dateTime('startDate');
            $table->dateTime('endDate');
            $table->string('coordinators');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('courses');
    }
};
