<?php

namespace Database\Factories;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;
use Ramsey\Uuid\Uuid;

class CourseFactory extends Factory
{
    public function definition(): array
    {
        $now = Carbon::now('utc');
        $courseLengthinDays = fake()->randomDigit();
        return [
            'id' => Uuid::uuid4(),
            'name' => Str::random(10),
            'description' => Str::random(20),
            'studyLoad' => fake()->randomDigit(),
            'level' => "master",
            'courseLengthInDays' => $courseLengthinDays,
            'startDate' => $now,
            'endDate' => $now->addDays($courseLengthinDays),
            'coordinators' => Str::random(10)
        ];
    }
}
