Course API - Laravel

## 1. Getting Started

First, install packages:

```bash
composer install
```

Secondly, copy .env.example to .env and modify following variables

DB_DATABASE=
DB_USERNAME=
DB_PASSWORD=

Third, generate APP_KEY

```bash
php artisan key:generate
```

## 2. Docker Development server

```bash
docker-compose up -d --build
```

## 3. API endpoint

Open [http://localhost:8000](http://localhost:8000) with your browser to see the result.

## Swagger

Open [http://localhost:8000/api/documentation](http://localhost:8000/api/documentation) with your browser to see the result.
