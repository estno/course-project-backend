FROM php:8.2
RUN apt-get update -y && apt-get install -y openssl zip unzip git
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
WORKDIR /app
COPY . /app
RUN docker-php-ext-install mysqli pdo pdo_mysql
RUN composer install
RUN chmod +x /app/bootstrap.sh
CMD ["/app/bootstrap.sh"]
EXPOSE 9000
