<?php

namespace App\Http\Controllers;

use Exception;
use App\Http\Requests\CoursePostRequest;
use App\Http\Requests\CourseUpdateRequest;
use App\Interfaces\CourceServiceInterface;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Response;

/**
 * @OA\Info(title="Course API", version="0.1")
 **/

class CourseController extends Controller
{

    public function __construct(private CourceServiceInterface $courseService) {}

    /**
     * @OA\Get(
     *     path="/api/courses",
     *     summary="Get a list of courses",
     *     tags={"Courses"},
     *     @OA\Response(response=200, description="Successful operation"),
     *     @OA\Response(response=400, description="Invalid request"),
     * )
     */
    public function index()
    {
        try {
            return response()->json([
                'courses' => $this->courseService->getAll()
            ]);
        }
        catch(Exception $ex) {
            return response()->json([
                'error' => $ex->getMessage()
            ], $ex->getCode());
        }
    }

    /**
     * @OA\Post(
     *     path="/api/courses",
     *     summary="Store a course",
     *     tags={"Courses"},
     *     @OA\Response(response=200, description="Successful operation"),
     *     @OA\Response(response=400, description="Invalid request"),
     *     @OA\RequestBody(
     *         required=true,
     *         description="Course data",
     *         @OA\JsonContent(
     *             required={"name", "studyLoad", "level", "startDate", "courseLengthInDays", "coordinators"},
     *             @OA\Property(property="name", type="string", example="Course Title"),
     *             @OA\Property(property="description", type="string", example="Course Description"),
     *             @OA\Property(property="studyLoad", type="integer", example=1),
     *             @OA\Property(property="level", type="string", example="Beginner"),
     *             @OA\Property(property="courseLengthInDays", type="integer", example=30),
     *             @OA\Property(property="startDate", type="string", format="date", example="2024-04-21"),
     *             @OA\Property(property="coordinators", type="string", example="John Doe"),
     *         )
     *     ),
     * )
     */
    public function store(CoursePostRequest $request)
    {
        try {
            $course = $this->courseService->save($request->validated());
            return response()->json([
                "course" => $course
            ]);
        }
        catch(Exception $ex) {
            return response()->json([
                'error' => $ex->getMessage()
            ], $ex->getCode());
        }
    }

    /**
     * @OA\Get(
     *      path="/api/courses/{id}",
     *      operationId="getCourseById",
     *      tags={"Courses"},
     *      summary="Get a course by ID",
     *      description="Returns a single course",
     *      @OA\Parameter(
     *          name="id",
     *          in="path",
     *          description="ID of the course",
     *          required=true,
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="course",
     *                  type="object",
     *                  description="Course details"
     *              )
     *          )
     *      ),
     *      @OA\Response(
     *          response=404,
     *          description="Course not found",
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="error",
     *                  type="string",
     *                  description="Error message"
     *              )
     *          )
     *      )
     * )
     */
    public function show(string $id): JsonResponse
    {
        try {
            return Response::json([
                'course' => $this->courseService->getById($id)
            ]);
        }
        catch(Exception $ex) {
            return response()->json([
                'error' => $ex->getMessage()
            ], $ex->getCode());
        }
    }

    /**
     * @OA\Put(
     *     path="/api/courses/{id}",
     *     summary="Update a course",
     *     tags={"Courses"},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="ID of the course to update",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Response(response=200, description="Successful operation"),
     *     @OA\Response(response=400, description="Invalid request"),
     *     @OA\RequestBody(
     *         required=true,
     *         description="Updated course data",
     *         @OA\JsonContent(
     *             required={"name", "studyLoad", "level", "startDate", "courseLengthInDays", "coordinators"},
     *             @OA\Property(property="name", type="string", example="Updated Course Title"),
     *             @OA\Property(property="description", type="string", example="Updated Course Description"),
     *             @OA\Property(property="studyLoad", type="integer", example=2),
     *             @OA\Property(property="level", type="string", example="Intermediate"),
     *             @OA\Property(property="courseLengthInDays", type="integer", example=45),
     *             @OA\Property(property="startDate", type="string", format="date", example="2024-04-21"),
     *             @OA\Property(property="coordinators", type="string", example="Jane Doe"),
     *         )
     *     ),
     * )
     */
    public function update(CourseUpdateRequest $request, string $id)
    {
        try {
            $course = $this->courseService->save($request->validated(), $id);
            return response()->json([
                'course' => $course
            ]);
        }
        catch(Exception $ex) {
            return response()->json([
                'error' => $ex->getMessage()
            ], $ex->getCode());
        }
    }

    /**
     * @OA\Delete(
     *     path="/api/courses/{id}",
     *     summary="Delete a course",
     *     tags={"Courses"},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="ID of the course to delete",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Response(response=200, description="Successful operation"),
     *     @OA\Response(response=400, description="Invalid request"),
     * )
     */
    public function destroy(string $id)
    {
        try {
            $this->courseService->remove($id);
            return response()->json([]);
        }
        catch(Exception $ex) {
            return response()->json([
                'error' => $ex->getMessage()
            ], $ex->getCode());
        }
    }
}
