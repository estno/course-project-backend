<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CoursePostRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'name' => 'required|max:200',
            'description' => 'sometimes|string|max:2000',
            'studyLoad' => 'required|integer|min:0|max:30',
            'level' => 'required|string|in:bachelor,master,doctoral',
            'courseLengthInDays' => 'required|integer|max:365',
            'startDate' => 'required|date|after_or_equal:today',
            'coordinators' => 'required',
        ];
    }
}
