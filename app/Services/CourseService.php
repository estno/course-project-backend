<?php

namespace App\Services;

use Exception;
use Illuminate\Support\Collection;

use App\Interfaces\CourceServiceInterface;
use App\Models\Course;
use Carbon\Carbon;
use Ramsey\Uuid\Uuid;

class CourseService implements CourceServiceInterface
{

    public function getAll(): Collection
    {
        return Course::all();
    }

    public function getById($id): Course
    {
        $course = Course::findOrFail($id);
        return $course == null ? new Course() : $course;
    }

    public function save(array $data, $id = null): Course
    {
        $endDate = Carbon::parse($data["startDate"]);
        $endDate->addDays($data["courseLengthInDays"] + 1);

        $course = $id == null ? new Course() : Course::findOrFail($id);

        if ($id == null) {
            $course->id = Uuid::uuid4();
        }

        $course->name = $data['name'];
        $course->description = $data['description'];
        $course->studyLoad = $data['studyLoad'];
        $course->level = $data['level'];
        $course->startDate = Carbon::parse($data["startDate"])->addDays(1)->format('Y-m-d');
        $course->endDate = $endDate->format('Y-m-d');
        $course->courseLengthInDays = $data['courseLengthInDays'];
        $course->coordinators = $data['coordinators'];
        $saved = $course->save();

        if (!$saved) {
            throw new \InvalidArgumentException("Error occurred while saving data");
        }

        return $course;
    }

    public function store(array $data): Course
    {
        $endDate = Carbon::parse($data["startDate"]);
        $endDate->addDays($data["courseLengthInDays"] + 1);

        $course = new Course();
        $course->id = Uuid::uuid4();
        $course->name = $data['name'];
        $course->description = $data['description'];
        $course->studyLoad = $data['studyLoad'];
        $course->level = $data['level'];
        $course->startDate = Carbon::parse($data["startDate"])->addDays(1)->format('Y-m-d');
        $course->endDate = $endDate->format('Y-m-d');
        $course->courseLengthInDays = $data['courseLengthInDays'];
        $course->coordinators = $data['coordinators'];
        $saved = $course->save();

        if (!$saved) {
            throw new \InvalidArgumentException("Error occurred while inserting data");
        }

        return $course;
    }

    public function update($data, $id): Course
    {
        $endDate = Carbon::parse($data["startDate"]);
        $endDate->addDays($data["courseLengthInDays"]);

        $course = Course::where('id', $id)->first();
        $course->name = $data['name'];
        $course->description = $data['description'];
        $course->studyLoad = $data['studyLoad'];
        $course->level = $data['level'];
        $course->startDate = $data['startDate'];
        $course->endDate = $endDate->format('Y-m-d H:i:s');
        $course->courseLengthInDays = $data['courseLengthInDays'];
        $course->coordinators = $data['coordinators'];
        $saved = $course->save();

        if (!$saved) {
            throw new \InvalidArgumentException("Kursust lisamisel tekis viga");
        }

        return $course;
    }

    public function remove($id): bool
    {
        return Course::destroy($id);
    }
}
