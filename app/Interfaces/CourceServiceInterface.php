<?php

namespace App\Interfaces;

use App\Models\Course;
use Illuminate\Support\Collection;

interface CourceServiceInterface
{
    public function getAll(): Collection;
    public function getById($id): Course;
    public function save(array $data, $id = null): Course;
    public function update($data, $id): Course;
    public function store(array $data): Course;
    public function remove($id): bool;
}
