<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Models\Course;
use App\Services\CourseService;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Collection;

class CourseServiceTest extends TestCase
{
    use RefreshDatabase;

    protected $courseService;

    public function setUp(): void
    {
        parent::setUp();

        $this->courseService = new CourseService();
    }

    /** @test */
    public function getAllCourses()
    {
        Course::factory()->count(3)->create();

        $courses = $this->courseService->getAll();

        $this->assertInstanceOf(Collection::class, $courses);
        $this->assertCount(3, $courses);
    }

    /** @test */
    public function getCourseById()
    {
        $course = Course::factory()->create();

        $retrievedCourse = $this->courseService->getById($course->id);

        $this->assertInstanceOf(Course::class, $retrievedCourse);
        $this->assertEquals($course->id, $retrievedCourse->id);
    }

    /** @test */
    public function saveNewCourse()
    {
        $data = [
            'name' => 'New Course',
            'description' => 'Description of new course',
            'studyLoad' => 1,
            'level' => 'master',
            'startDate' => now('utc')->format('Y-m-d'),
            'courseLengthInDays' => 5,
            'coordinators' => 'Juku',
        ];

        $savedCourse = $this->courseService->save($data);

        $this->assertInstanceOf(Course::class, $savedCourse);
        $this->assertNotNull($savedCourse->id);
        $this->assertEquals($data['name'], $savedCourse->name);
    }

    /** @test */
    public function updateCourse()
    {
        $existingCourse = Course::factory()->create();

        $data = [
            'name' => 'Updated Course Name',
            'description' => 'Updated course description',
            'studyLoad' => 2,
            'level' => 'master',
            'startDate' => now('utc')->format('Y-m-d'),
            'courseLengthInDays' => 7,
            'coordinators' => 'Juku',
        ];

        $updatedCourse = $this->courseService->save($data, $existingCourse->id);

        $this->assertInstanceOf(Course::class, $updatedCourse);
        $this->assertEquals($existingCourse->id, $updatedCourse->id);
        $this->assertEquals($data['name'], $updatedCourse->name);
    }

    /** @test */
    public function removeCourse()
    {
        $course = Course::factory()->create();

        $isDeleted = $this->courseService->remove($course->id);

        $this->assertTrue($isDeleted);
        $this->assertDatabaseMissing('courses', ['id' => $course->id]);
    }
}
