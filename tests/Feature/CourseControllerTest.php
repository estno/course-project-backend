<?php

namespace Tests\Feature;

use App\Models\Course;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class CourseControllerTest extends TestCase
{

    use RefreshDatabase;

    /**
     * @test
     */
    public function it_returns_courses_as_json(): void
    {
        $courses = Course::factory()->count(3)->create();
        $response = $this->getJson(route('courses.index'));
        $response
            ->assertStatus(200)
            ->assertJson([
                'courses' => $courses->toArray(),
            ]);
    }

    /**
     * @test
     */
    public function it_returns_course_as_json(): void
    {
        $course = Course::factory()->count(3)->create();
        $response = $this->getJson('/api/courses/' . $course->first()->id);
        $response
            ->assertStatus(200)
            ->assertJson([
                'course' => $course->first()->toArray(),
            ]);
    }

    /** @test */
    public function it_can_store_a_course()
    {
        $data = [
            'name' => 'New Course',
            'description' => 'This is a new course description.',
            'studyLoad' => 1,
            'level' => 'master',
            'courseLengthInDays' => 3,
            'startDate' => Carbon::now('utc')->toDateTimeString(),
            'coordinators' => 'Juku'
        ];

        $response = $this->postJson(route('courses.store'), $data);

        $response
            ->assertStatus(200)
            ->assertJsonStructure([
                'course' => [
                    'id',
                    'name',
                    'description',
                    'studyLoad',
                    'level',
                    'courseLengthInDays',
                    'startDate',
                    'endDate',
                    'coordinators',
                ],
            ]);

        //$this->assertDatabaseHas('courses', $data);
    }

    /** @test */
    public function it_can_update_a_course()
    {
        $data = [
            'name' => 'New Course',
            'description' => 'This is a new course description.',
            'studyLoad' => 1,
            'level' => 'master',
            'courseLengthInDays' => 3,
            'startDate' => Carbon::now('utc')->toDateTimeString(),
            'coordinators' => 'Juku'
        ];

        $response = $this->postJson(route('courses.store'), $data);
        $decodedData = json_decode($response->baseResponse->getContent());

        $updatedData = [
            'id' => $decodedData->course->id,
            'name' => 'New Course',
            'description' => 'This is a new course description.',
            'studyLoad' => 1,
            'level' => 'master',
            'courseLengthInDays' => 3,
            'startDate' => Carbon::now('utc')->toDateTimeString(),
            'coordinators' => 'Juku'
        ];

        $response = $this->putJson('/api/courses/' . $decodedData->course->id, $updatedData);

        $response->assertStatus(200);
    }

    /** @test */
    public function it_handles_invalid_course_id()
    {
        $invalidId = 'invalid_id';

        $updatedData = [
            'id' => $invalidId,
            'name' => 'New Course',
            'description' => 'This is a new course description.',
            'studyLoad' => 1,
            'level' => 'master',
            'courseLengthInDays' => 3,
            'startDate' => Carbon::now('utc')->toDateTimeString(),
            'coordinators' => 'Juku'
        ];

        $response = $this->putJson('/api/courses/' . $invalidId, $updatedData);

        $response->assertStatus(500);
    }
}
